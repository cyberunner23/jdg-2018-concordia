import timeit

def sort():

    start_time = timeit.default_timer()
    numbers = None
    with open('to_be_sorted.txt', 'r') as f:
        lines = f.readlines()
        numbers = [None]*len(lines)
        for i in range(len(lines)):
            numbers[i] = int(lines[i])

    numbers = sorted(numbers)

    with open('sorted.txt', 'w') as f:
        for n in numbers:
            f.write(str(n) + '\n')
    elapsed = timeit.default_timer() - start_time
    print(elapsed)


if __name__ == '__main__':
    sort()

def sorted(numbers):
	
    n = len(numbers)
    m = 999999 +1
    count = [0] * m               # init with zeros
    for a in numbers:
        count[a] += 1             # count occurences
    i = 0
    for a in range(m):            # emit
        for c in range(count[a]): # - emit 'count[a]' copies of 'a'
            numbers[i] = a
            i += 1
    return numbers
