import json
from itertools import permutations
from flask import Flask
import flask
from flask import jsonify
#from flask import jsonify

#Create a flask app
app = Flask(__name__)

#Permutation function
def doPermutation(data):
    return list(set(map(" ".join, permutations(data))))



#Route the prtmutations endpoint, return the JSONified permutations
@app.route('/permutations', methods=['POST'])
def permutate():
	
    flask.request.json.get('input', "")
	
    return jsonify({'result': doPermutation(flask.request.json.get('input', "").replace(" ", ""))})
	
	
	
	#4return "JSON Message: " + json.dumps(flask.request.json)
	
#Start a flask app
if __name__ == '__main__':
    app.run(debug=True)
	
# call to test the api
# curl --header "Content-Type: application/json" --request POST '{"input": "1 2"}' http://127.0.0.1:5000/permutations	