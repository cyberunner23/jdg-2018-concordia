Requirements

 - python libs:
   - Flask
   
To run the server
 - create an environment variable: FLASK_APP=permutations.py
 
 To test the server
  - with the curl command: curl --header "Content-Type: application/json" -H "Accept: application/json" -X POST --data '{"input": "**INPUT DATA**"}' http://127.0.0.1:5000/permutations
  
  where **INPUT DATA** is the input string